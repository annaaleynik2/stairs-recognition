#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv4/imgproc/imgproc.hpp>
#include <opencv4/highgui.hpp>
#include <stdio.h>

using namespace cv;
using namespace std;

static const std::string OPENCV_WINDOW = "Image window";
static const float focalLength = 476.703084;
static const float sensorHeigth = 4000;
// static const float imageHeigth = 708;
static const float imageHeigth = 800;
// static const float distance = 4900;
static const float distanceToObj = 4000;


class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/my_robot_2/camera1/image_raw", 1,
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    destroyWindow(OPENCV_WINDOW);
  }

  static bool comp (Vec4i a, Vec4i b) {
    return a[1] > b[1];
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    Mat img = cv_ptr->image;\

    cv::rotate(img, img, cv::ROTATE_90_CLOCKWISE);

    imshow("Source",img);

    GaussianBlur(img,img,Size(5,5),0,0);

    Mat dst, out_img,control;
    Canny(img, dst, 80, 240, 3);
    cvtColor(dst, out_img, cv::COLOR_GRAY2BGR);
    cvtColor(dst, control, cv::COLOR_GRAY2BGR);

    vector<int> y_keeper_for_lines;
    vector<Vec4i> corectLines;
    vector<Vec4i> lines;
    HoughLinesP(dst, lines, 1, CV_PI/180, 30, 40, 5 );

    for( size_t i = 1; i < lines.size(); i++ )
    {
        Vec4i l = lines[i];
        line( control, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, cv::LINE_AA);
    }

    Vec4i l = lines[0];
    line( out_img, Point(0, l[1]), Point(img.cols, l[1]), Scalar(0,0,255), 3, cv::LINE_AA);
    y_keeper_for_lines.push_back(l[1]);
    corectLines.push_back(l);

    int okey = 1;
    int stair_counter = 1;

    for( size_t i = 1; i < lines.size(); i++ )
    {
        Vec4i l = lines[i];
        for(int m:y_keeper_for_lines)
        {
            if(abs(m-l[1])<15)
                okey = 0;

        }
        if(okey)
        {
            line( out_img, Point(0, l[1]), Point(img.cols, l[1]), Scalar(0,0,255), 3, cv::LINE_AA);
            y_keeper_for_lines.push_back(l[1]);
            stair_counter++;
            corectLines.push_back(l);
        }
        okey = 1;

    }

    float h;

    sort(corectLines.begin(), corectLines.end(), comp);

    float objectHeight; 
    float dist = distanceToObj;
    vector<float> stepWidth(corectLines.size() - 1);
    for (int i = 0; i < corectLines.size() - 1; i++) {
      objectHeight = abs(corectLines[i][1] - corectLines[i + 1][1]);
      stepWidth[i] = dist * objectHeight * sensorHeigth / (focalLength * imageHeigth);
      putText(out_img,"Stair height " + to_string(i + 1) + ": " + to_string(stepWidth[i]),Point(40,100 + 40 * i),FONT_HERSHEY_SIMPLEX,1.5,Scalar(0,255,0),2);
      dist += 500.0f;
    }

    imshow(OPENCV_WINDOW, out_img);
    
    waitKey(3);

    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}